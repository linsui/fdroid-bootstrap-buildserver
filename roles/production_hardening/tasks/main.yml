---

- name: "copy: global git config to prevent usage"
  copy:
    src: etc_gitconfig
    dest: /etc/gitconfig
    mode: 0444
    owner: root
    group: root

- name: "copy: git config for automation"
  copy:
    src: gitconfig
    dest: "{{ userhome }}/.gitconfig"
    mode: 0444
    owner: root
    group: root

- name: "copy: {{ user }} ssh config for deploying"
  copy:
    src: ssh.config
    dest: "{{ userhome }}/.ssh/config"
    mode: 0440
    owner: root
    group: "{{ user }}"

- name: "file: lock down {{ user }} rc files"
  file:
    path: "{{ item }}"
    owner: root
    group: root
    mode: a+rX,a-w
  with_items:
    - "{{ userhome }}/.bash_logout"
    - "{{ userhome }}/.bashrc"
    - "{{ userhome }}/.gitconfig"
    - "{{ userhome }}/.profile"
    - "{{ userhome }}/.ssh/authorized_keys"
    - "{{ userhome }}/.ssh/"
    - "{{ userhome }}/"

- name: "file: make {{ userhome }}/.ansible"
  file:
    path: "{{ userhome }}/.ansible"
    state: directory
    mode: 0755
    owner: "{{ user }}"
    group: "{{ user }}"

- name: "file: make {{ userhome }}/.cache"
  file:
    path: "{{ userhome }}/.cache"
    state: directory
    mode: 0755
    owner: "{{ user }}"
    group: "{{ user }}"

- name: "file: make {{ userhome }}/.emacs.d"
  file:
    path: "{{ userhome }}/.emacs.d"
    state: directory
    mode: 0750
    owner: root
    group: "{{ user }}"

- name: "copy: {{ userhome }}/.emacs.d/init.el"
  copy:
    content: |
      ;; managed by Ansible
      (setq column-number-mode t)
      (setq inhibit-startup-screen t)
      (setq user-emacs-directory-warning nil)
      (setq visible-bell t)
      (show-paren-mode t)
    dest: "{{ userhome }}/.emacs.d/init.el"
    mode: 0750
    owner: root
    group: "{{ user }}"

- name: "file: make {{ userhome }}/.vagrant.d"
  file:
    path: "{{ userhome }}/.vagrant.d"
    state: directory
    mode: 0755
    owner: "{{ user }}"
    group: "{{ user }}"

- name: "alternatives: set vi as default editor"
  alternatives:
    name: editor
    path: /usr/bin/vim.basic

- name: "copy: set emacs as sensible-editor for {{ user }}"
  copy:
    content: |
      # maintained in Ansible
      SELECTED_EDITOR="/usr/bin/vim.basic"
    dest: "{{ userhome }}/.selected_editor"
    mode: 0644
    owner: root
    group: root

- name: "copy: set emacs as sensible-editor for root"
  copy:
    content: |
      # maintained in Ansible
      SELECTED_EDITOR="/usr/bin/vim.basic"
    dest: /root/.selected_editor
    mode: 0600
    owner: root
    group: root

- name: "file: lock down {{ userhome }}/fdroiddata/.git/config so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroiddata/.git/config"
    mode: 0644
    owner: root
    group: root
- name: "file: lock down {{ userhome }}/fdroiddata/.git/hooks so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroiddata/.git/hooks"
    state: directory
    mode: 0755
    owner: root
    group: root
- name: "find: collect files in {{ userhome }}/fdroiddata/.git/hooks"
  find:
    paths: "{{ userhome }}/fdroiddata/.git/hooks"
    hidden: True
    recurse: True
    file_type: any
  register: collected_files
- name: "file: remove all files in {{ userhome }}/fdroiddata/.git/hooks"
  file:
    path: "{{ item.path }}"
    state: absent
  with_items: "{{ collected_files.files }}"
- name: "git_config: fetch fdroiddata over tor"
  git_config:
    repo: "{{ userhome }}/fdroiddata"
    name: http.proxy
    value: http://localhost:8081
    scope: local
- name: "git_config: do not follow git redirects for fdroiddata"
  git_config:
    repo: "{{ userhome }}/fdroiddata"
    name: http.followRedirects
    value: "false"
    scope: local

- name: "file: lock down {{ userhome }}/fdroiddata/binary_transparency/.git/config so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroiddata/binary_transparency/.git/config"
    mode: 0644
    owner: root
    group: root
- name: "file: lock down {{ userhome }}/fdroiddata/binary_transparency/.git/hooks so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroiddata/binary_transparency/.git/hooks"
    state: directory
    mode: 0755
    owner: root
    group: root
- name: "find: collect files in {{ userhome }}/fdroiddata/binary_transparency/.git/hooks"
  find:
    paths: "{{ userhome }}/fdroiddata/binary_transparency/.git/hooks"
    hidden: True
    recurse: True
    file_type: any
  register: collected_files
- name: "file: remove all files in {{ userhome }}/fdroiddata/binary_transparency/.git/hooks"
  file:
    path: "{{ item.path }}"
    state: absent
  with_items: "{{ collected_files.files }}"

- name: "file: lock down {{ userhome }}/fdroidserver/.git/config so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroidserver/.git/config"
    mode: 0644
    owner: root
    group: root
- name: "file: lock down {{ userhome }}/fdroidserver/.git/hooks so {{ user }} no write access"
  file:
    path: "{{ userhome }}/fdroidserver/.git/hooks"
    state: directory
    mode: 0755
    owner: root
    group: root
- name: "find: collect files in {{ userhome }}/fdroidserver/.git/hooks"
  find:
    paths: "{{ userhome }}/fdroidserver/.git/hooks"
    hidden: True
    recurse: True
    file_type: any
  register: collected_files
- name: "file: remove all files in {{ userhome }}/fdroidserver/.git/hooks"
  file:
    path: "{{ item.path }}"
    state: absent
  with_items: "{{ collected_files.files }}"
- name: "git_config: fetch fdroidserver over tor"
  git_config:
    repo: "{{ userhome }}/fdroidserver"
    name: http.proxy
    value: http://localhost:8081
    scope: local
- name: "git_config: do not follow git redirects for fdroidserver"
  git_config:
    repo: "{{ userhome }}/fdroidserver"
    name: http.followRedirects
    value: "false"
    scope: local
